package fr.froz.AriaServ;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class Main {
    private static String[] getPlayerInfo(String UUID){
        if(!Files.exists(Paths.get("players/"+UUID+".arinfo"))){
            try {
                PrintWriter pw = new PrintWriter(new FileWriter("players/"+UUID+".arinfo"));
                pw.print("player%0%0");
                pw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        File playerFile = new File("players/"+UUID+".arinfo");
        String playerInfoS = "";
        try {
            byte[] bytes = Files.readAllBytes(playerFile.toPath());
            playerInfoS = new String(bytes,"UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return playerInfoS.split("%");
    }

    public static void main(String[] args) throws Exception{
        String clientSentence;
        ServerSocket welcomeSocket = new ServerSocket(6789);

        while (true) {

            StringBuilder content;
            Socket connectionSocket = welcomeSocket.accept();
            BufferedReader inFromClient =
                    new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
            DataOutputStream outToClient = new DataOutputStream(connectionSocket.getOutputStream());
            clientSentence = inFromClient.readLine();
            System.out.println("INFO [REQUEST]: " + clientSentence);
            String[] clientRequest = clientSentence.split("£");
            if (clientRequest[0].equalsIgnoreCase("config")) {
                List<String> lines = Files.readAllLines(Paths.get("config/"+clientRequest[1] + ".yml"), Charset.forName("UTF-8"));
                int i = 1;
                content = new StringBuilder(lines.get(0));
                while (i < lines.size()) {
                    content.append("£").append(lines.get(i));
                    i++;
                }
                String toSend = content.toString() + '\n';
                outToClient.writeBytes(toSend);
                connectionSocket.close();
            } else if(clientRequest[0].equalsIgnoreCase("perms")) {
                String[] playerInfo = getPlayerInfo(clientRequest[1]);
                List<String> lines = Files.readAllLines(Paths.get("perms/" + playerInfo[0] + ".ariaperm"), Charset.forName("UTF-8"));
                int i = 1;
                content = new StringBuilder(lines.get(0));
                while (i < lines.size()) {
                    content.append("£").append(lines.get(i));
                    i++;
                }
                String toSend = content.toString() + '\n';
                outToClient.writeBytes(toSend);
                connectionSocket.close();
            } else if (clientRequest[0].equalsIgnoreCase("money")) {
                String[] playerInfo = getPlayerInfo(clientRequest[1]);
                if (clientRequest[2].equalsIgnoreCase("get")) {
                    if (clientRequest[3].equalsIgnoreCase("AriaPoints")) {
                        content = new StringBuilder(playerInfo[1]);
                        String toSend = content.toString() + '\n';
                        outToClient.writeBytes(toSend);
                        connectionSocket.close();
                    } else if (clientRequest[3].equalsIgnoreCase("AriaCoins")) {
                        content = new StringBuilder(playerInfo[2]);
                        String toSend = content.toString() + '\n';
                        outToClient.writeBytes(toSend);
                        connectionSocket.close();
                    }
                } else if (clientRequest[2].equalsIgnoreCase("add")) {
                    if (clientRequest[3].equalsIgnoreCase("AriaPoints")) {
                        PrintWriter pw = new PrintWriter(new FileWriter("players/" + clientRequest[1] + ".arinfo"));
                        pw.write(playerInfo[0] + "%" + String.valueOf(Integer.valueOf(playerInfo[1]) + Integer.valueOf(clientRequest[4])) + "%" + playerInfo[2]);
                        pw.close();
                        content = new StringBuilder(String.valueOf(Integer.valueOf(playerInfo[1]) + Integer.valueOf(clientRequest[4])));
                        String toSend = content.toString() + '\n';
                        outToClient.writeBytes(toSend);
                        connectionSocket.close();
                    } else if (clientRequest[3].equalsIgnoreCase("AriaCoins")) {
                        PrintWriter pw = new PrintWriter(new FileWriter("players/" + clientRequest[1] + ".arinfo"));
                        pw.write(playerInfo[0] + "%" + playerInfo[1] + "%" + String.valueOf(Integer.valueOf(playerInfo[2]) + Integer.valueOf(clientRequest[4])));
                        pw.close();
                        content = new StringBuilder(String.valueOf(Integer.valueOf(playerInfo[2]) + Integer.valueOf(clientRequest[4])));
                        String toSend = content.toString() + '\n';
                        outToClient.writeBytes(toSend);
                        connectionSocket.close();
                    }
                }
            } else if (clientRequest[0].equalsIgnoreCase("grade")) {
                String[] playerInfo = getPlayerInfo(clientRequest[1]);
                PrintWriter pw = new PrintWriter(new FileWriter("players/" + clientRequest[1] + ".arinfo"));
                pw.write(clientRequest[2] + "%" + playerInfo[1] + "%" + playerInfo[2]);
                pw.close();
                connectionSocket.close();
            }
        }

    }
}
